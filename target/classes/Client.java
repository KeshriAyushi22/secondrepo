

	package com.ayushi.ClientFL;

	import java.io.BufferedReader;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.net.HttpURLConnection;
	import java.net.URL;

	public class Client {
		//http connection --> get method
		
		public static void main(String args[]) throws IOException {
			try {
		String url = "http://localhost:8282/FL/web/api/Solar";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();

		con.setRequestMethod("GET");
		con.setRequestProperty("Accept", "application/text");
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		int responseCode = con.getResponseCode();
		if (con.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ con.getResponseCode());
		}


		System.out.println("\nSending 'GET' request to URL : " + url);
		System.out.println("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(
				new InputStreamReader(con.getInputStream()));  
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
			}
			catch(Exception e) {
				System.out.println(e);
			}

		}
	}


}
